<?php
define("PAGEID", "villapark");
require('include/header.php');
?>

<section class="villapark-content">

	<section class="boxes">
		<img src="" data-src="holder.js/940x395" alt="">

	</section>

	<section class="line">
		<a class="button-blue-right" href="#">Fotogalerie</a>

		<h1>Vážení hosté, vítejte na stránkách Villaparku Happy Hill</h1>
	</section>

	<article class="from-editor">
		<section id="tab1" class="tab-villapark">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto assumenda commodi consequatur cumque eius esse est, expedita explicabo inventore ipsa nihil numquam quae quibusdam quis quod repellat sint tenetur vel!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias animi beatae <strong>cumque dolorem et, expedita impedit in maxime molestias natus nihil porro quaerat rem sapiente suscipit</strong>. Consectetur doloribus molestiae praesentium!</p>
			<p class="blue">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque fugit id ipsum praesentium</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus at cumque eum hic id inventore, iste molestiae mollitia neque numquam, praesentium rem, reprehenderit soluta suscipit ut vel veniam voluptate!</p>
		</section>

		<section id="tab2" class="tab-villapark">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias animi beatae <strong>cumque dolorem et, expedita impedit in maxime molestias natus nihil porro quaerat rem sapiente suscipit</strong>. Consectetur doloribus molestiae praesentium!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto assumenda commodi consequatur cumque eius esse est, expedita explicabo inventore ipsa nihil numquam quae quibusdam quis quod repellat sint tenetur vel!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus at cumque eum hic id inventore, iste molestiae mollitia neque numquam, praesentium rem, reprehenderit soluta suscipit ut vel veniam voluptate!</p>
			<p class="blue">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque fugit id ipsum praesentium</p>
		</section>

		<section id="tab3" class="tab-villapark">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto assumenda commodi consequatur cumque eius esse est, expedita explicabo inventore ipsa nihil numquam quae quibusdam quis quod repellat sint tenetur vel!</p>
			<p class="blue">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque fugit id ipsum praesentium</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus at cumque eum hic id inventore, iste molestiae mollitia neque numquam, praesentium rem, reprehenderit soluta suscipit ut vel veniam voluptate!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias animi beatae <strong>cumque dolorem et, expedita impedit in maxime molestias natus nihil porro quaerat rem sapiente suscipit</strong>. Consectetur doloribus molestiae praesentium!</p>
		</section>
	</article>

	<aside class="left">
		<nav class="tabs">
			<ul>
				<li><a href="#tab1">Léto</a></li>
				<li><a href="#tab2">Zima</a></li>
				<li><a href="#tab3">Hřiště</a></li>
				<li><a href="#tab4">Bazén</a></li>
				<li><a href="#tab5">Okolí</a></li>
				<li><a href="#tab6">Video</a></li>
			</ul>
		</nav>
	</aside>

</section>



<?php
require('include/footer.php');
?>