<?php
define("PAGEID", "contacts");
require('include/header.php');
?>

<section class="contacts-content">
	<div id="map-canvas"></div>

	<article class="from-editor">
		<h1>Villapark Happy Hill</h1>
		<p class="address">
			Čistá v Krkonoších <br>
			CZ - 543 44 Černý Důl
		</p>

		<h2>správa rekreačního areálu:</h2>
		<p class="admins">
			East Bohemia Trading, s.r.o. <br>
			Čistá v Krkonoších 87 <br>
			CZ - 543 44 Černý Důl <br>
			IČ: 25964798 <br>
			DIČ: CZ25964798
		</p>
		<p>
			tel: 00420 499 692357 <br>
			kontaktní e-mail: <a title="kontaktní e-mail" href="mailto:info@ebt.cz">info@ebt.cz</a>
		</p>

		<h2>pronájem domků</h2>
		<p>
			rezervace: <a title="Rezervace" href="mailto:booking@happ-hill.cz">booking@happy-hill.cz</a><br>
			číslo pro ubytované hosty <br>
			v případě nouze: 00420 603 217 012
		</p>
	</article>

	<aside class="left">
		<img src="images/zoover-rating.jpg" alt=""/>
		<img src="images/qr.png" alt=""/>
	</aside>

	<aside class="right">
		<section class="boxes">
			<section>
				<div class="outer">
					<a href="#">
						<h3 style="color: #97bee5">Fotogalerie</h3>
						<img src="." data-src="holder.js/187x177/social" alt="">
					</a>
				</div>
				<div class="outer">
					<a href="#">
						<h3 style="color: #fff">Tipy na výlety</h3>
						<img src="." data-src="holder.js/187x177/dark" alt="">
					</a>
				</div>
			</section>
		</section>

	</aside>
</section>



<?php
require('include/footer.php');
?>