<!doctype html>
<!--[if IE 8]>         <html class="no-js lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="format-detection" content="telephone=no">
	<title>happyhill.cz</title>


	<link rel="stylesheet" href="css/normalize.css" />
	<link rel="stylesheet" href="css/main.css" />
	<link rel="stylesheet" href="css/libs/jquery-ui-1.10.3.custom.css">

	<script src="scripts/libs/modernizr.js"></script>
</head>
<!--
v zavislosti na jayzkove verzi je treba doplnit tridu tagu BODY
moznosti, se kterymi se v JS pocita jsou: cs, en, de
-->
<body class="cs <?php echo defined('PAGEID') ? PAGEID : 'DEFAULT TITLE'; ?>">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/cs_CZ/all.js#xfbml=1&appId=275052985974619";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="container">

	<header class="top">
		<div class="inner">
			<h1 id="logo"><a href="homepage.php"><img src="images/logo.png" alt="Happy Hill"></a></h1>
			<nav class="main">
				<ul>
					<?php
					/*
					zacatek noveho tagu LI v menu musi vzdy zacinat na stejnym radku, jako konci ten predchozi
					jinak se v prohlizeci zobrazuje mezera mezi polozkami
					*/
					?>
					<li<?php if(PAGEID == 'villapark') echo " class='active'"?>>
					<a href="villapark.php">Villapark</a></li><li<?php if(PAGEID == 'gallery') echo " class='active'"?>>
					<a href="gallery-2.php">Domky</a><ul class="submenu">
						<li><a href="#">item 1</a></li>
						<li><a href="#">item 2</a></li>
						<li><a href="#">item 3</a></li>
					</ul></li><li<?php if(PAGEID == 'gallery') echo " class='active'"?>>
					<a href="gallery.php">Fotogalerie</a></li><li<?php if(PAGEID == 'pricelist') echo " class='active'"?>>
					<a href="pricelist.php">Ceník</a></li><li<?php if(PAGEID == 'contacts') echo " class='active'"?>>
					<a href="contacts.php">Kontakty</a></li><li<?php if(PAGEID == 'reviews') echo " class='active'"?>>
					<a href="reviews.php">Hodnocení</a></li>
				</ul>
			</nav>
			<div class="quick-contact">
				<address>
					Čistá v Krkonoších<br />
					CZ - 543 44 Černý Důl
				</address>
				<span class="phone">00420 499 692 357</span>
				<a href="mailto:recepce@happyhill.cz" title="Recepce"></a>
			</div>
			<nav class="languages">
				<ul>
					<li><a href="#">eng</a></li>
					<li><a href="#">de</a></li>
					<li class="current"><a href="#">cze</a></li>
				</ul>
			</nav>
		</div>
	</header>

	<div class="inner">


		<section class="main">
			<div class="inner">