			</div>  <!-- / .main > .inner -->
		</section>

		<?php if( defined('PAGEID') && PAGEID != 'homepage' && PAGEID != 'pricelist' && PAGEID != 'order'): ?>
			<aside class="panel">
				<h2>Termíny 2013</h2>
				<div class="icons">
					<span class="ico-1" title="WiFi"></span>
					<span class="ico-2" title="Rodiny s dětmi"></span>
					<span class="ico-3" title="Bazén"></span>
					<span class="ico-6" title="Domácí mazlíci"></span>
					<span class="ico-4" title="Lyžování"></span>
					<span class="ico-5" title="Skibus"></span>
					<span class="ico-7" title="Cyklovýlety"></span>
					<span class="ico-8" title="Golfík"></span>
				</div>

				<div class="columns">
					<div class="reservation">
						<a href="pricelist.php"><span>Zarezervuj</span></a>
						<p>Cena za pronájem domu <strong>za 1 den</strong></p>
					</div>
					<div>
						<h3>Červen</h3>
						<ul>
							<li>
								<a href="pricelist.php"><span class="date">16. 6.</span>
									<span class="price">
										<span class="span price-old">&euro;55,-</span>
										<strong class="price-new">&euro;45,-</strong>
									</span></a>
							</li>
							<li>
								<a href="pricelist.php"><span class="date">16. 6.</span>
									<span class="price">
										<span class="span price-old">&euro;55,-</span>
										<strong class="price-new">&euro;45,-</strong>
									</span></a>
							</li>
						</ul>
					</div>
					<div>
						<h3>Červenec</h3>
						<ul>
							<li>
								<a href="pricelist.php"><span class="date">16. 6. - 23. 6.</span>
									<span class="price">
										<span class="span price-old">&euro;55,-</span>
										<strong class="price-new">&euro;45,-</strong>
									</span></a>
							</li>
							<li>
								<a href="pricelist.php"><span class="date">16. 6.</span>
									<span class="price">
										<span class="span price-old">&euro;55,-</span>
										<strong class="price-new">&euro;45,-</strong>
									</span></a>
							</li>
							<li>
								<a href="pricelist.php"><span class="date">16. 6.</span>
									<span class="price">
										<span class="span price-old">&euro;55,-</span>
										<strong class="price-new">&euro;45,-</strong>
									</span></a>
							</li>
						</ul>
					</div>
					<div>
						<h3>Srpen</h3>
						<ul>
							<li>
								<a href="pricelist.php"><span class="date">16. 6.</span>
									<span class="price">
										<span class="span price-old">&euro;55,-</span>
										<strong class="price-new">&euro;45,-</strong>
									</span></a>
							</li>
							<li>
								<a href="pricelist.php"><span class="date">16. 6.</span>
									<span class="price">
										<span class="span price-old">&euro;55,-</span>
										<strong class="price-new">&euro;45,-</strong>
									</span></a>
							</li>
							<li>
								<a href="pricelist.php"><span class="date">16. 6.</span>
									<span class="price">
										<span class="span price-old">&euro;55,-</span>
										<strong class="price-new">&euro;45,-</strong>
									</span></a>
							</li>
							<li>
								<a href="pricelist.php"><span class="date">16. 6.</span>
									<span class="price">
										<span class="span price-old">&euro;55,-</span>
										<strong class="price-new">&euro;45,-</strong>
									</span></a>
							</li>
							<li>
								<a href="pricelist.php"><span class="date">16. 6.</span>
									<span class="price">
										<span class="span price-old">&euro;55,-</span>
										<strong class="price-new">&euro;45,-</strong>
									</span></a>
							</li>
						</ul>
					</div>
					<div>
						<h3>Září</h3>
						<ul>
							<li>
								<a href="pricelist.php"><span class="date">16. 6.</span>
									<span class="price">
										<span class="span price-old">&euro;55,-</span>
										<strong class="price-new">&euro;45,-</strong>
									</span></a>
							</li>
							<li>
								<a href="pricelist.php"><span class="date">16. 6.</span>
									<span class="price">
										<span class="span price-old">&euro;55,-</span>
										<strong class="price-new">&euro;45,-</strong>
									</span></a>
							</li>
						</ul>
					</div>

					<a href="pricelist.php  " class="arrow-reservation"></a>

				</div>
			</aside>
		<?php elseif(defined('PAGEID') && (PAGEID == 'pricelist' || PAGEID == 'order')): ?>
			<aside class="icons-blue">
				<div class="ico-grid">
					<em><span class="ico-1" title="WiFi"></span></em>
					<em><span class="ico-2" title="Rodiny s dětmi"></span></em>
					<em><span class="ico-3" title="Bazén"></span></em>
					<em><span class="ico-6" title="Domácí mazlíci"></span></em>
					<em><span class="ico-4" title="Lyžování"></span></em>
					<em><span class="ico-5" title="Skibus"></span></em>
					<em><span class="ico-7" title="Cyklovýlety"></span></em>
					<em><span class="ico-8" title="Golfík"></span></em>
				</div>
			</aside>

		<?php endif ?>

		<footer class="bottom">
			<section class="links">
				<div class="column">
					<h4>Villapark</h4>
					<ul>
						<li><a href="#">areál</a></li>
						<li><a href="#">rekerační domky</a></li>
					</ul>
				</div>
				<div class="column">
					<h4>Fotogalerie</h4>
					<ul>
						<li><a href="#">léto</a></li>
						<li><a href="#">zima</a></li>
						<li><a href="#">domek</a></li>
					</ul>
				</div>
				<div class="column">
					<h4>Ceník &amp; rezervace</h4>
					<ul>
						<li><a href="#">ceník</a></li>
						<li><a href="#">informace pro hosty</a></li>
						<li><a href="#">rezervace</a></li>
						<li><a href="#">rezervační podmínky</a></li>
					</ul>
				</div>
				<div class="column">
					<h4>Tipy na výlet</h4>
					<ul>
						<li><a href="#">Adršpach</a></li>
						<li><a href="#">ZOO Dvůr Králové</a></li>
						<li><a href="#">Skiareál Černý Důl</a></li>
					</ul>
				</div>
				<div class="column">
					<h4>Kontakty</h4>
					<address>
						Čistá v Krkonoších <br />
						CZ - 543 44 Černý Důl <br />
						00420 499 692 357 <br />
						<a href="mailto:recepce@happyhill.cz" title="Recepce">recepce@happyhill.cz</a>
					</address>
				</div>
			</section>

			<section class="meta">
				<p class="copy">&copy; 2013 Villapark Happy Hill, Všechna práva vyhrazena</p>
				<div class="social-footer">

					<div class="social-frame-fb">
						<div class="fb-like" data-href="http://www.happyhill.cz" data-width="The pixel width of the plugin" data-height="The pixel height of the plugin" data-colorscheme="light" data-layout="button_count" data-action="like" data-show-faces="true" data-send="false"></div>
					</div>

					<div class="social-frame-tw">
						<a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
					</div>

					<div class="social-frame-pin">
						<a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.happyhill.cz&amp;media=http%3A%2F%2Ftestuje.cz%2Fhappyhill2%2Fimages%2Fimg-trips.jpg&amp;description=Villa%20Park%20Happy%20Hill" data-pin-do="buttonPin" data-pin-config="none"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" alt=""></a>
						<script type="text/javascript">
						(function(d){
						  var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
						  p.type = 'text/javascript';
						  p.async = true;
						  p.src = '//assets.pinterest.com/js/pinit.js';
						  f.parentNode.insertBefore(p, f);
						}(document));
						</script>
					</div>

					<div class="social-frame-gplus">
						<!-- Place this tag where you want the share button to render. -->
						<div class="g-plus" data-action="share" data-annotation="none"></div>
						<!-- pokud sdilet jinou nez aktualni URL, staci doplnit parametr "data-href", viz: https://developers.google.com/+/web/share/-->

						<!-- Place this tag after the last share tag. -->
						<script type="text/javascript">
						  (function() {
						    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
						    po.src = 'https://apis.google.com/js/plusone.js';
						    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
						  })();
						</script>
					</div>

				</div>
				<ul>
					<li><a href="#">Disclaimer</a></li>
					<li><a href="#">Privacy</a></li>
					<li><a href="#">Mapa stránek</a></li>
				</ul>
			</section>
		</footer>

		<div class="logos">
			<ul>
				<li><a href="#"><img src="images/logo-tripadvisor.png" alt=""></a></li>
				<li><a href="#"><img src="images/logo-zoover.png" alt=""></a></li>
				<li><a href="#"><img src="images/logo-holidaycheck.png" alt=""></a></li>
			</ul>
		</div>

	</div>  <!-- / .container > .inner -->

	<?php if( defined('PAGEID') && PAGEID == 'homepage'): ?>
	<div class="main-slider">

		<article class="promo">
			<div>
				<header>
					<h1>Rekreační domky k pronájmu</h1>
				</header>
				<p>WWW Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe, nemo, ab, ipsum, vitae voluptates excepturi quidem earum ea aperiam neque placeat magnam ipsam dolores quaerat qui hic error asperiores vero! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore impedit in molestias nam omnis quidem quo recusandae repellat, reprehenderit tempora. Doloribus et minima natus perspiciatis ut. Esse nostrum placeat tempora?</p>
				<p><a href="#" class="more">Více info</a></p>
			</div>
			<div class="promo-images">
				<div class="inner">
					<ul>
						<!-- může a nemusí být odkazy -->
						<li><img alt="" src="images/zoover-rating.jpg"></li>
						<li><a href="#"><img alt="" src="." data-src="holder.js/150x180/social"></a></li>
						<li><img alt="" src="." data-src="holder.js/150x180/industrial"></li>
					</ul>
				</div>
			</div>
		</article>

		<div class="inner">

			<article class="slide" id="slide-1">
				<a href="#" class="positioned-promo" style="top: 110px; left: 50%; margin-left: -405px;"><img src="images/promo-box-slider.png" alt=""/></a>
				<img src="." data-src="holder.js/1920x640/social" alt="" class="visual">
			</article>
			<article class="slide" id="slide-2">
				<a href="#"><img src="." data-src="holder.js/1920x640/industrial" alt="" class="visual"></a>
			</article>
			<article class="slide" id="slide-3">
				<a href="#"><img src="." data-src="holder.js/1920x640/gray" alt="" class="visual"></a>
			</article>
		</div>
	</div>
	<?php endif ?>

	<span class="shadow-top"></span>

	<div class="social-bar">
		<ul>
			<li><a href="http://www.facebook.com/happyhill.vakantiepark" target="_blank" class="soc-button-fb"><img src="images/soc-fb.png" alt=""></a></li>
			<li><a href="https://twitter.com/parkhappyhill" target="_blank" class="soc-button-tw"><img src="images/soc-tw.png" alt=""></a></li>
			<li><a href="mailto:aaa@bbb.com?subject=Subject%20Text%20Here&amp;body=Hello%20everyone!!%0D%0AThis%20is%20your%20body%20text." class="soc-button-em"><img src="images/soc-em.png" alt=""></a></li>
			<li><a href="https://www.pinterest.com" target="_blank" class="soc-button-pin"><img src="images/soc-pin.png" alt=""></a></li>
			<li><a href="http://plus.google.com/u/0/b/108575815524105098020/108575815524105098020/posts" target="_blank" class="soc-button-gplus"><img src="images/soc-gplus.png" alt=""></a></li>
		</ul>
	</div>

</div> <!-- / .container -->

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/_libs/_js/jquery-1.9.1.js"><\/script>')</script>

<!--[if (gte IE 6)&(lte IE 8)]>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
<![endif]-->

<script src="scripts/libs/holder.js"></script>

<script src="scripts/utils.js"></script>

<script src="scripts/libs/jquery.minimalect.js"></script>

<!-- homepage -->
<script src="scripts/main-slider.js"></script>
<script src="scripts/homepage.js"></script>

<!-- galerie -->
<script src="scripts/libs/jquery.roundabout.js"></script>
<script src="scripts/libs/jquery.roundabout-shapes.min.js"></script>
<script src="scripts/libs/jquery.event.drag-2.2.js"></script>
<script src="scripts/libs/jquery.event.drop-2.2.js"></script>
<script src="scripts/gallery.js"></script>

<!-- cenik -->
<!-- nevim proc ale ten carousel se nejak nema rad s jqueryui, ktery se pouziva v kalendari, takze tyhle dva skripty radeji nenacitat najednou-->
<?php if( defined('PAGEID') && PAGEID == 'pricelist'): ?>
<!--<script src="scripts/libs/jquery-ui-1.10.3.custom.js"></script>-->
<script src="scripts/libs/moment.min.js"></script>
<script src="scripts/libs/jquery.ui.core.js"></script>
<script src="scripts/libs/jquery.ui.datepicker.js"></script>
<script src="scripts/libs/jquery.multidatespicker.js"></script>
<script src="scripts/pricelist.js"></script>
<?php endif ?>

<!-- kontakty + mapa -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

<!-- YT -->
<script>
	// Load the IFrame Player API code asynchronously.
	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/player_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
</script>

<script src="scripts/script.js"></script>

</body>
</html>