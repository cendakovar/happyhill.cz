var MAINSLIDER = {
	cache: {
		currentStep: 0,
		revSlideWidth: 290,
		revSlideHeight: 217,
		revSlideSpace: 16
	}
};

window.MAINSLIDER = MAINSLIDER;

MAINSLIDER.init = function( elem ) {

	MAINSLIDER.cache.slider = elem; //$( '.main-slider' );
	MAINSLIDER.cache.bodywidth = $(document).width();
	MAINSLIDER.cache.isMain = elem.hasClass( 'main-slider' );

	window.elem = elem;

	var controls = $( '<a href="#prev" class="main-slider-controls slider-prev"></a><a href="#next" class="main-slider-controls slider-next"></a>'),
		controlsContainer = elem.hasClass( 'main-slider') ? MAINSLIDER.cache.slider : MAINSLIDER.cache.slider.parent();

	MAINSLIDER.cache.offsetWidth = MAINSLIDER.cache.isMain ? MAINSLIDER.cache.bodywidth : MAINSLIDER.cache.revSlideWidth + MAINSLIDER.cache.revSlideSpace;

	//MAINSLIDER.cache.slider.append( controls )

	controls.appendTo( controlsContainer );

	$('.slider-prev, .slider-next', controlsContainer).on( 'click', function( evt ) {
		var $this = $(this);

		evt.preventDefault();

		MAINSLIDER.move( $this.hasClass( 'slider-prev' ) ? 'prev' : 'next' );
	} );

	if( !MAINSLIDER.cache.isMain ) {
		elem.children( '.inner' ).height( MAINSLIDER.cache.revSlideHeight );
	}

	MAINSLIDER.scale();
}

MAINSLIDER.scale = function() {
	var temp = MAINSLIDER.cache.slider.find( '.slide' );
	MAINSLIDER.cache.slidesCount = temp.length;
	MAINSLIDER.cache.inner = MAINSLIDER.cache.slider.children( '.inner' );

	temp.each(function() {
		$(this).width( MAINSLIDER.cache.isMain ? MAINSLIDER.cache.bodywidth : MAINSLIDER.cache.revSlideWidth  );
	})

	MAINSLIDER.cache.inner.width( MAINSLIDER.cache.slidesCount * MAINSLIDER.cache.bodywidth );

}

MAINSLIDER.move = function( direction ) {

	switch( direction ) {
		case 'prev':
			--MAINSLIDER.cache.currentStep;
			break;

		default:
			++MAINSLIDER.cache.currentStep
	}

	if( MAINSLIDER.cache.currentStep >= MAINSLIDER.cache.slidesCount - (MAINSLIDER.cache.isMain ? 0 : 2)) {
		MAINSLIDER.cache.currentStep = 0;
	}

	if( MAINSLIDER.cache.currentStep < 0) {
		MAINSLIDER.cache.currentStep = MAINSLIDER.cache.slidesCount - (MAINSLIDER.cache.isMain ? 1 : 3);
	}


	MAINSLIDER.cache.inner.animate( {
		left: -MAINSLIDER.cache.currentStep * MAINSLIDER.cache.offsetWidth + 'px'
	} );
}

$(function() {
	if($( '.main-slider' ).length) {
		MAINSLIDER.init( $('.main-slider') );
	}

	if($( '.reviews-slider' ).length) {
		MAINSLIDER.init( $('.reviews-slider') );
	}
})