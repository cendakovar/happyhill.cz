var Utils = {
	cache: {}
};

window.Utils = Utils;

// these are (ruh-roh) globals. You could wrap in an
// immediately-Invoked Function Expression (IIFE) if you wanted to...
Utils.cache.currentTallest = 0;
Utils.cache.currentRowStart = 0;
Utils.cache.rowDivs = new Array();


Utils.sameHeight = {}

	Utils.sameHeight.setConformingHeight = function(el, newHeight) {
		// set the height to something new, but remember the original height in case things change
		el.data("originalHeight", (el.data("originalHeight") == undefined) ? (el.height()) : (el.data("originalHeight")));
		el.height(newHeight);
	};

	Utils.sameHeight.getOriginalHeight = function(el) {
		// if the height has changed, send the originalHeight
		return (el.data("originalHeight") == undefined) ? (el.height()) : (el.data("originalHeight"));
	};

	Utils.sameHeight.columnConform = function( elems ) {
		// find the tallest DIV in the row, and set the heights of all of the DIVs to match it.
		elems.each(function() {

			// "caching"
			var $el = $(this);

			var topPosition = $el.position().top,
				currentDiv;

			if (Utils.cache.currentRowStart != topPosition) {

				// we just came to a new row.  Set all the heights on the completed row
				for(currentDiv = 0 ; currentDiv < Utils.cache.rowDivs.length ; currentDiv++) Utils.sameHeight.setConformingHeight(Utils.cache.rowDivs[currentDiv], Utils.cache.currentTallest);

				// set the variables for the new row
				Utils.cache.rowDivs.length = 0; // empty the array
				Utils.cache.currentRowStart = topPosition;
				Utils.cache.currentTallest = Utils.sameHeight.getOriginalHeight($el);
				Utils.cache.rowDivs.push($el);

			} else {

				// another div on the current row.  Add it to the list and check if it's taller
				Utils.cache.rowDivs.push($el);
				Utils.cache.currentTallest = (Utils.cache.currentTallest < Utils.sameHeight.getOriginalHeight($el)) ? (Utils.sameHeight.getOriginalHeight($el)) : (Utils.cache.currentTallest);

			}
			// do the last row
			for (currentDiv = 0 ; currentDiv < Utils.cache.rowDivs.length ; currentDiv++) {
				Utils.sameHeight.setConformingHeight(Utils.cache.rowDivs[currentDiv], Utils.cache.currentTallest);
			}

		});

	};


Utils.tabs = {
	cache: {
		selector: '.tabs',
		currentTab: 0
	},
	init: function() {


		if($(Utils.tabs.cache.selector).length <= 0) {
			return;
		}


		var selector = $(Utils.tabs.cache.selector);

		Utils.tabs.cache.ul = selector.find('nav ul');
		Utils.tabs.cache.tabsLength = selector.find('nav li').length;


		Utils.tabs.cache.ul.prepend('<li><a href="#tab-prev"><</a></li>');
		Utils.tabs.cache.ul.append('<li><a href="#tab-next">></a></li>');

		$('a', Utils.tabs.cache.ul).on('click', function(evt) {
			var $this = $(this);

			Utils.tabs.move($this.attr('href'))

			evt.preventDefault();
		});
	},
	move: function(index) {

		if(index == '#tab-prev') {
			if(Utils.tabs.cache.currentTab == 0) {
				var lastItem = Utils.tabs.cache.ul.find('a[href="#tab-next"]').parent().prev().find('a').attr('href');
				Utils.tabs.cache.currentTab = Utils.tabs.cache.ul.find('li').length - 2;
				Utils.tabs.move(lastItem);
			} else {
				Utils.tabs.cache.currentTab--;
				var prevItem = Utils.tabs.cache.ul.find('.active').prev().find('a').attr('href');
				Utils.tabs.move(prevItem);
			}

			return false;
		}

		if(index == '#tab-next') {
			if(Utils.tabs.cache.currentTab == Utils.tabs.cache.tabsLength) {
				var firstItem = Utils.tabs.cache.ul.find('a[href="#tab-prev"]').parent().next().find('a').attr('href');
				Utils.tabs.cache.currentTab = 0;
				Utils.tabs.move(firstItem);

			} else {
				Utils.tabs.cache.currentTab++;
				var nextItem = Utils.tabs.cache.ul.find('.active').next().find('a').attr('href');
				Utils.tabs.move(nextItem);
			}
			return false;
		}

		$('div.active', $(Utils.tabs.cache.selector)).fadeOut(300, function() {
			$(this).removeClass('.active');
			$('li.active', $(Utils.tabs.cache.selector)).removeClass('active');
		});
		$(index).fadeIn(320, function() {
			$(this).addClass('active').removeAttr('style');
			$('a[href="' + index + '"]').parent().addClass('active');
		});


		return false;
	}
};