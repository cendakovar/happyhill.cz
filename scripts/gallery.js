$(function() {
	Holder.add_theme("dark", {background:"#000", foreground:"#666", size: 10})

	var gallery = $( '.gallery,.reviews' ),
		carousel = gallery.find( '.gallery-thumbs' ),
		placeholder = gallery.find( '.gallery-placeholder' ),
		reviewsContainer = gallery.find( '.reviews-container' ),
		changeImage = function( position ) {

			var newElem;

			if( gallery.hasClass('gallery')) {

				var imageSource = carousel.find('li:nth-child(' + position + ') img').attr('data-target');

				newElem = $('<img>', {
								'src': imageSource,
								'alt': ''
							});

				placeholder.empty().append( newElem );

				Holder.run();
			} else {
				var data = carousel.find('li:nth-child(' + position + ')').data('src')

				newElem = reviewsContainer.find('#' + data).clone();

				placeholder.empty().append( newElem );
			}
		};

	carousel.roundabout( {
		btnNext: '.next',
		btnPrev: '.prev',
		shape: 'waterWheel',
		enableDrag: 'true',
		dragAxis: 'y',
		dragFactor: 2,
		minScale: .3
//		,
//		triggerFocusEvents: false,
//		dropCallback: function() {
//			console.log('stop');
//		}
	}, function() {
		//console.log( 'roundabout ready' );
		// placeholder.empty().append( carousel.find( 'li:first img' ).clone() );
		changeImage( 1 );
	} ).on('animationEnd', function() {
		//console.log('stop2');
		changeImage( carousel.roundabout("getChildInFocus") + 1 );
	});

});