var Pricelist = {};

window.Pricelist = Pricelist;

Pricelist.cache = {
	body: $('body'),
	// kolik roku bude v selectboxu nad kalendarem
	maxYOffsetForward: 3,
	// o kolik mesicu se lze tedy presunout vpred
	maxMOffsetForward: 36,
	// pocet aktualne odsunutych mesicu
	currentOffset: 0,
	// pole uz zabranych datumu, ktere nelze znova nakliknout (definovano primo ve strance)
	disabled: window.disabledDefinition,
	disabledMoment: [],
	// pole datumu se specialni akci (definovano primo ve strance)
	special: window.specialsDefinition,
	// dnesni datum
	today: new Date(),
	// posun mesicu sem a tam
	btnPrev: $('.load-prev'),
	btnNext: $('.load-next'),
	datepickerElement: $( '#datepicker' ),
	dateFormats: {
		'cs': 'dd.mm.yy',
		'de': 'dd.mm.yy',
		'en': 'mm/dd/yy'
	}
};

// lokalizacni texty + formaty
$.datepicker.regional['cs'] = {
	closeText: 'Zavřít',
	prevText: 'Předchozí',
	nextText: 'Další',
	currentText: 'Nyní',
	monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen', 'Červenec','Srpen','Září','Říjen','Listopad','Prosinec'],
	monthNamesShort: ['Le','Ún','Bř','Du','Kv','Čn', 'Čc','Sr','Zá','Ří','Li','Pr'],
	dayNames: ['Neděle','Pondělí','Úterý','Středa','Čtvrtek','Pátek','Sobota'],
	dayNamesShort: ['Ne','Po','Út','St','Čt','Pá','So'],
	dayNamesMin: ['Ne','Po','Út','St','Čt','Pá','So'],
	weekHeader: 'Sm',
	dateFormat: Pricelist.cache.dateFormats.cs,
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: ''
};

$.datepicker.regional['de'] = {
	prevText: '&#x3c;zurück', prevStatus: '',
	prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
	nextText: 'Vor&#x3e;', nextStatus: '',
	nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
	currentText: 'heute', currentStatus: '',
	todayText: 'heute', todayStatus: '',
	clearText: '-', clearStatus: '',
	closeText: 'schließen', closeStatus: '',
	monthNames: ['Januar','Februar','März','April','Mai','Juni',
	'Juli','August','September','Oktober','November','Dezember'],
	monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun',
	'Jul','Aug','Sep','Okt','Nov','Dez'],
	dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
	dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
	dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
	showMonthAfterYear: false,
	//dateFormat:'d MM, y'
	dateFormat: Pricelist.cache.dateFormats.de
};




Pricelist.customDates = function ( date ) {

	// if(Pricelist.cache.disabledMoment.length == 0) {
	// 	for(var i = 0; i < Pricelist.cache.disabled.length; i++) {
	// 		Pricelist.cache.disabledMoment.push(moment(Pricelist.cache.disabled[i]));
	// 	}
	// }

	// pokud uz uzivatel nekdy predtim vybiral typ terminu (vlastni, tyden, vikend)
	// tak to ma ulozeno u sebe v localStorage a ten typ se mu ted vybere
	// jako defaultni je nastaven tyden
	var localValue = localStorage.getItem('calendarRange') || 'week';


	for (var i = 0; i < Pricelist.cache.special.length; i++) {
		if (date.getTime() == Pricelist.cache.special[i].getTime()) {

			// nevim jestli specialni akce pri vikendovym nebo tydennim pobytu zobrazovat cely, nebo jen soboty
			if( (localValue == 'custom') || (localValue == 'weekend' && date.getDay() == 6) || (localValue == 'week' && date.getDay() == 6)) {
				return [true, 'special', 'special date'];
			}
		}
	}


	Utils.cache.localValue = localValue;

	if( localValue == 'custom' ) {

		Pricelist.multiConfig.mode = 'normal';

		return [true, ''];

	} else if (localValue == 'weekend') {

		// pokud je to vikend, vybere se vzdy aktualne nakliknuty den + 2 nasledujici
		Pricelist.multiConfig.mode = 'daysRange';
		Pricelist.multiConfig.autoselectRange = [0, 3];

		var x = date.getDay();
		if(x == 6) {
			// var xyz = Pricelist.checkFutureInterval(date, 2);
			// console.log(xyz);
			// console.log(date);
			// console.log(Pricelist.cache.disabled.indexOf(date));
		}

		// navic je povoleno kliknout pouze na patek
		return [ (x == 5 && Pricelist.checkFutureInterval(date, 3)), "" ];

	} else {

		// pokud je nastaven cely tyden, vybira se aktualne naklinut yden + 7 nasledujicich dnu
		Pricelist.multiConfig.mode = 'daysRange';
		Pricelist.multiConfig.autoselectRange = [0, 8];

		var x = date.getDay();

		// tydenni pobyty jsou od soboty do soboty, cili je opet povolena pouze sobota
		return [ (x == 6 && Pricelist.checkFutureInterval(date, 8)), "" ];

	}

};

// pokud pole zakazanych datumu neobsahuje nektery z datumu v intervalu je vraceno true, jinak false
Pricelist.checkFutureInterval = function( date, length ) {
	var out = true;

	if(Pricelist.cache.disabled.length == 0) {
		return out;
	}

	for(var j = 0; j < Pricelist.cache.disabled.length; j++) {
		if(Pricelist.cache.disabled[j].getTime() > date.getTime() && Pricelist.cache.disabled[j].getTime() < date.getTime() + length*24*60*60*1000 ) {
			//alert(Pricelist.cache.disabled[j]);
			//alert(date);
			out = false
		}
	}


	return out;
};

Pricelist.multiConfig = {
	minDate: new Date(),
	beforeShowDay: Pricelist.customDates,
	firstDay: 1, // pondeli
	numberOfMonths: 3,
	//showCurrentAtPos: 1,
	// nasledujici dve promenne se nastavi az v zavoslosti na typu vyberu - tyden / vikend/ vlastni
	//mode: 'daysRange',
	//autoselectRange: '', // eg: [0,7] - nastaveno, aby se vybiralo vzdy 7 dnu od aktualniho
	addDisabledDates: Pricelist.cache.disabled.length > 0 ? Pricelist.cache.disabled : null, // pole jiz zabranych terminu, pokud zadne nejsou, zada se prazdna hodnota
	// addDates: Pricelist.cache.defaultDates || false,
	onSelect: function( clicked, inst ) {

		// kontrola, jestli je pro kliknuty mesic mozno vybrat vlastni datum,
		// nebo pouze tydenni / vikendovy pobyt

		var x = Pricelist.checkInterval(clicked);

		// bylo zruseno vybrani vsech datumu v kalendari, ma se neco stat?
		if(Pricelist.cache.datepickerElement.multiDatesPicker('getDates').length === 0) {

			// POKUD JE TYP VYBERU NASTVEN NA VLASTNI:
			// if (Pricelist.cache.datepickerElement.datepicker( 'option', 'mode' ) == 'daysRange') {
			// 	alert($(this).data('with-disabled-date'));
			// }

			// pokud uzivatel mel vybrane nejake datum a vse zase odznacil
			// alert($(this).data('unselected-all'));

		} else if(x !== true) {
			alert($(this).data('only-' + x));
			Pricelist.cache.datepickerElement.multiDatesPicker('resetDates', 'picked');
		} else {
			// vypis vybranych datumu do konzole
			console.log(Pricelist.cache.datepickerElement.multiDatesPicker('getDates'));
		}

	},
	adjustRangeToDisabled: false
};

// funkce pro prevod ruznych formatu data na jeden spolecny
Pricelist.normalizeDate = function( date ) {
	var _date,
		tempFormat;

	switch(Pricelist.cache.lang) {
		case 'cs':
			tempFormat = Pricelist.cache.dateFormats.cs.toUpperCase();
			break;

		case 'en':
			tempFormat = Pricelist.cache.dateFormats.en.toUpperCase();
			break;

		case 'de':
			tempFormat = Pricelist.cache.dateFormats.de.toUpperCase();
			break;
	}

	_date = moment(date, tempFormat);

	return _date;
};

// kontrola, zda v danem mesici lze vybrat pouze
// tydenni / vikendovy / vlastni termin
Pricelist.checkInterval = function( date ) {
	var d = Pricelist.normalizeDate( date ),
		//month = parseInt(d.split(".")[1]),
		month = d.month() + 1,	// kvuli tomu, ze mesice jsou indexovany od 0, takze rijen je tady jako 9
		out;// = {};
		// TODO: bude zalezet na tom, jake datum je to spolecne, ted to funguje pro typicke ceske datum dd.mm.yyyy

	switch(localStorage.getItem('calendarRange')) {
		case 'custom':
			// if( monthsDefinition.week.indexOf( month ) > -1) {
			if( $.inArray( month, monthsDefinition.week ) > -1) {
				out = "week";
				return out;
			}
			// if ( monthsDefinition.weekandweekend.indexOf( month ) > -1 ) {
			if ( $.inArray( month, monthsDefinition.weekandweekend ) > -1 ) {
				out = "weekandweekend";
				return out;
			}
			return true;
			break;

		case 'weekend':
			//if( monthsDefinition.week.indexOf( month ) > -1 ) {
			if( $.inArray( month, monthsDefinition.week ) > -1 ) {
				out = "week";
				return out;
			}
			return true;
			break;

		default:
			out = true;
			return out;
			break;
	}
};

// resi skrivani / zobrazovani tlacitek pro posun kalendare (mesicu)
Pricelist.handleMovementButtons = function() {
	if(Pricelist.cache.currentOffset <= 0 ) {
		Pricelist.cache.btnPrev.fadeOut( 300 );
	} else {
		Pricelist.cache.btnPrev.fadeIn( 300 );
	}

	if(Pricelist.cache.currentOffset >= Pricelist.cache.maxMOffsetForward ) {
		Pricelist.cache.btnNext.fadeOut( 300 );
	} else {
		Pricelist.cache.btnNext.fadeIn( 300 );
	}
};

Pricelist.moveMonths = function( direction ) {

	switch( direction ) {
		case 'prev':
			if( Pricelist.cache.currentOffset <= 0 ) {
				break;
			}
			Pricelist.cache.currentOffset--;
			break;
		case 'next':
			if( Pricelist.cache.currentOffset >= Pricelist.cache.maxMOffsetForward ) {
				break;
			}
			Pricelist.cache.currentOffset++;
			//$( '#datepicker' ).datepicker( 'setDate', '+1m');
	}

	// posun kalendare po mesici sem nebo tam
	Pricelist.cache.datepickerElement.datepicker( 'setDate', '+' + Pricelist.cache.currentOffset + 'm');

	Pricelist.handleMovementButtons()
};

// automaticky vybrane datum
// tady pouze ukazka jak se to da udelat
Pricelist.defaultDates = function() {
	Pricelist.cache.defaultDates = false;
	if(window.location.href.indexOf('dateStart') == -1) {
		return false;
	}

	switch(Pricelist.cache.lang) {
		case 'en':
			Pricelist.cache.defaultDates = ['10/28/2013','10/29/2013'];
			break;

		case 'cs':
		case 'de':
			Pricelist.cache.defaultDates = ['28.10.2013','29.10.2013'];
			break;
	}

	// pridani datumu jako defaultne vybranych
	Pricelist.multiConfig.addDates = Pricelist.cache.defaultDates;

	localStorage.setItem('calendarRange', 'custom');

};

Pricelist.init = function( firstInit ) {

	// priznak, zda jde o prvni init kalendare
	// napr kdyz je defaultne vybrane nejake datum,
	// tak po zmene intervalu na tyden uz datum mit vybrane nechceme
	var firstInit = firstInit;

	if(Pricelist.cache.body.hasClass('cs')) {
		Pricelist.cache.lang = 'cs';
	}
	if(Pricelist.cache.body.hasClass('en')) {
		Pricelist.cache.lang = 'en';
	}
	if(Pricelist.cache.body.hasClass('de')) {
		Pricelist.cache.lang = 'de';
	}

	Pricelist.cache.datepickerElement.datepicker( 'destroy' );

	Pricelist.cache.datepickerElement.multiDatesPicker('resetDates', 'picked');

	// vychozi datum lze nastavit pouze pri prvnim nacteni, ne po zmene v selectboxu terminu
	if(firstInit !== false) {
		Pricelist.defaultDates();
	} else {
		Pricelist.multiConfig.addDates = null;
	}

	// tady bude potreba vyresit, jak nastavit jine nez ceske prostredi
	$.datepicker.setDefaults( $.datepicker.regional[ Pricelist.cache.lang ] );

	Pricelist.cache.datepickerElement.multiDatesPicker( Pricelist.multiConfig );


	$( ".pricelist-panel > a" ).off( 'click' ).on( 'click', function( evt ) {

		evt.preventDefault();

		var direction = $(this).hasClass( 'load-prev' ) ? 'prev' : 'next';

		Pricelist.moveMonths( direction );
	});

};

$(function() {
	if( Pricelist.cache.datepickerElement.length ) {
		Pricelist.init();

		// TODO: remove
		App.cache.body.append('<div class="dates-panel">Disabled items:<br><ul></ul></div>');
		for(var i = 0; i < Pricelist.cache.disabled.length; i++) {
			var d = moment(Pricelist.cache.disabled[i]);
			$('.dates-panel ul').append('<li>' + d.format('DD.MM.YYYY') + '</li>');
		}
	}
});