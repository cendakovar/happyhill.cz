var App = {};

App.debug = false;

App.cache = {
	body: $('body'),
	lang: ''
};

App.initSelectbox = function( elem ) {
	elem.each(function() {

		var $this = $(this),
			enableChange = false,
			config = {
				onopen: function() {
					enableChange = true;
				},
				onchange: function() {
					// funkce se zavola jen na strance s galerii
					if( $this.attr('id') == 'gallery-select' && enableChange) {
						var selector = $this.next('.minict_wrapper').find('.selected').data('value'),
							url = $this.find('option[value=' + selector + ']').data('url');

						window.location.href = url;
					}

					// funkce se zavola jen v ceniku pri zmene roku v kalendari
					if( $this.attr('id') == 'year-select' && enableChange) {

						var selector = $this.next('.minict_wrapper').find('.selected').data('value'),
							year = $this.find('option[value=' + selector + ']').data('url'),
							now = new Date(),
							delta = year - now.getFullYear();

						if(!!$this.data('prev-selected') && $this.data('prev-selected') == year) {

							//console.log($this.data('prev-selected'));
							return false;
						}


						$this.attr('data-prev-selected', year);

						//delta = delta == 0 ? -1 : delta;

						Pricelist.cache.currentOffset = ( delta * 12 );

						$('#datepicker').datepicker('setDate', '+' + delta + 'y');

						Pricelist.handleMovementButtons();
					}

					// funkce se zavola jen v ceniku pri zmene intervalu vyberu data
					if( $this.attr('id') == 'style-select' && enableChange) {
						var $t = $( '#' + $this.attr( 'id' ) );

						//console.log($t, $t.val());

						if( $t.val() == 'week' ) {

							localStorage.setItem('calendarRange', 'week');

						} else if( $t.val() == 'weekend' ) {

							localStorage.setItem('calendarRange', 'weekend');

						} else {

							localStorage.setItem('calendarRange', 'custom');

						}
						//console.log(localStorage.getItem('calendarRange'));
						Pricelist.init( false );
					}

					// if( $this.hasClass('form-minimalect') ) {

					// }
				}
			};




		if(!!$this.attr('data-text-empty')) {
			config.empty = $this.attr('data-text-empty');
		}
		if(!!$this.attr('data-text-placeholder')) {
			config.placeholder = $this.attr('data-text-placeholder');
		}

		$this.minimalect( config );

		if(!!$this.attr('data-default-value')) {
			$this.val( $this.attr('data-default-value') ).change();
		}

		if($this.attr('id') == 'style-select') {
			$("#style-select").val(Utils.cache.localValue).change();
		}
	});


};

App.initCheckbox = function( elem ) {
	elem.each( function() {
		var $this = $(this),
			parent = $this.parent();

		$this.after("<span></span>");

		if( $this.is( ':checked' ) ) {
			parent.addClass( 'checked' );
		}

		$this.off( 'change' ).on( 'change', function() {

			parent.toggleClass( 'checked', $(this).is(':checked') );

		} );
	});
} ;

App.init = function(){

	if(App.cache.body.hasClass('cs')) {
		App.cache.lang = 'cs';
	}
	if(App.cache.body.hasClass('en')) {
		App.cache.lang = 'en';
	}
	if(App.cache.body.hasClass('de')) {
		App.cache.lang = 'de';
	}

	var panelColumns = $('.panel .columns > div'),
		gallerySelect = $('#gallery-select' ),
		yearSelect = $('#year-select'),
		formMinimalect = $('.form-minimalect');

	if(panelColumns.length) {
		Utils.sameHeight.columnConform( panelColumns );
	}

	if(panelColumns.length) {
		App.initSelectbox( gallerySelect );
	}

	if(yearSelect.length) {
		App.initSelectbox( yearSelect );
	}

	if(formMinimalect.length) {
		App.initSelectbox( formMinimalect );
	}

	if( $('.chb-label').length ) {
		App.initCheckbox( $('.chb-label input') );
	}

	if(App.cache.body.hasClass('contacts') ) {
		var elems = $( '.contacts-content > article, .contacts-content > aside'),
			map,
			marker,
			position = new google.maps.LatLng(50.6363275,15.7100437),
			contentString,
			infoWindow,
			mapOptions = {
				zoom: 12,
				center: position,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				scrollwheel: false
			};

		Utils.sameHeight.columnConform( elems );

		function initialize() {

			map = new google.maps.Map(document.getElementById('map-canvas'),
				mapOptions);

			marker = new google.maps.Marker({
				position: position,
				map: map,
				title: 'Villapark Happy Hill'
			});

			///
			contentString = '<div class="map-bubble-content">'+
//				'<div id="siteNotice">'+
//				'</div>'+
				'<h3 class="firstHeading">Villapark Happy Hill</h3>'+
				'<div id="bodyContent">'+
				'<p class="address">'+
				'Čistá v Krkonoších <br>'+
				'CZ - 543 44 Černý Důl'+
				'</p>'+
//				'</div>'+
				'</div>';

			infoWindow = new google.maps.InfoWindow({
				content: contentString
			});
			///




			google.maps.event.addListener(marker, 'click', function() {
				infoWindow.open(map, marker);
			});
		}

		google.maps.event.addDomListener(window, 'load', initialize);

	}

	if( App.cache.body.hasClass('villapark') ) {
		var tabsNav = $('nav.tabs');

		$('.tabs li:first, #tab1').addClass('active');

		$('a', tabsNav).on( 'click', function( evt ) {
			var $this = $(this);
			evt.preventDefault();

			$('.tab-villapark.active').fadeOut(300, function() {
				$(this).removeClass('active');

				$('.tab-villapark').filter($this.attr('href')).fadeIn(150, function() {
					$(this).addClass('active');
				});
			});

			$('li', tabsNav ).removeClass('active');
			$this.closest('li').addClass('active');

		});
	}

	$('.social-bar').css({
		marginTop: -($('.social-bar').outerHeight() / 2) + 'px'
	});

	// je potreba pockat, nez se nactou socky
	setTimeout(function() {
		$('.social-footer').css({
			marginLeft: -($('.social-footer').outerWidth() / 2) + 'px'
		});
	}, 3000);

	// $('.soc-button-fb').on('click', function( evt ) {
	// 	evt.preventDefault();
	// });

	// $('.soc-button-gplus').on('click', function( evt ) {
	// 	evt.preventDefault();
	// });

	App.textPageTitle();
};

App.createPlayer = function(videoID) {
	var videoID = videoID;

	App.cache.body.append($('<div id="ytplayer-lightbox"><div id="ytplayer-wrapper"><div id="ytplayer"></div><a href="#" id="ytplayer-close">+</a></div></div>'));

//	App.cache.videoContainer = App.cache.body.find( '.video-container' );
	App.cache.videoContainer = App.cache.body.find( '.video-container' );

	App.cache.elementsHide = 'span, img';
	App.cache.elementsShow = '.overlay, #ytplayer-wrapper';

	// Replace the 'ytplayer' element with an <iframe> and
	// YouTube player after the API code downloads.
	onYouTubePlayerAPIReady = function() {
		App.player = new YT.Player('ytplayer', {
			width: '187',
			height: '177',
			// tady se doplni ID videa, ktery se ma prehravat
			videoId: videoID,
			playerVars: {
				'controls': 0,
				'enablejsapi': 1,
				'showinfo': 0,
				'wmode': "opaque",
				'rel': 0,
				'html5': 1
			}
		});
	};
};

App.playerStart = function() {
//	App.cache.videoContainer.find( App.cache.elementsHide ).fadeOut( 300 );

	App.cache.ytplayerLightbox = App.cache.ytplayerLightbox || $('#ytplayer-lightbox');

	App.cache.ytplayerLightbox.fadeIn(300);

	App.player.playVideo();
};

App.playerStop = function() {
//	App.cache.videoContainer.find( App.cache.elementsHide ).fadeIn( 300 );

	App.cache.ytplayerLightbox.fadeOut(300);

	App.player.pauseVideo();
};

App.initPlayer = function() {
	var container = App.cache.body.find( '.video-container' );
	if(container.length == 0) {
		return;
	}

	App.createPlayer(container.data('videoid'));

	$( '.video-container').on( 'click', function( evt ) {
		evt.preventDefault();
		App.playerStart();
		App.cache.ytplayerLightbox.addClass( 'playing' );
	});

	$( '#ytplayer-lightbox, #ytplayer-close' ).on( 'click', function( evt ) {
		evt.preventDefault();

		App.playerStop();

		App.cache.ytplayerLightbox.removeClass( 'playing' );
	});

};

App.textPageTitle = function() {
	if(App.cache.body.hasClass('textpage') && $('.title h1 a').length) {
		var h1 = $('.title h1'),
			buttonWidth = $('a', h1).outerWidth()

		h1.css({
			'padding-right': buttonWidth + 20 + 'px'
		});
	}
};

$(function() {
	App.init();

	App.initPlayer();

	Utils.tabs.init();
});