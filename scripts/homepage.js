var Homepage = {};

Homepage.promoSlider = function() {
	var promoCounter = 0,
		promoImagesLength = $( '.promo-images li' ).length,
		promoSlider = $( '.promo-images ul' ),
		promoSlideWidth = 160,
		delta = 0;

	promoSlider.width( promoImagesLength * promoSlideWidth );
	$( 'li', promoSlider ).width( promoSlideWidth );

	setInterval( function() {
		if( promoCounter % promoImagesLength == 0) {
			delta = 0
			promoCounter = 0
		} else {
			delta = -promoCounter * promoSlideWidth
		}
		promoCounter++

		promoSlider.animate({
			marginLeft: delta + 'px'
		}, 300);
	}, 3300 );
};

Homepage.reviews = function() {
	var slider = $('.slider'),
		sliderUl = slider.find( 'ul' ),
		reviewsCount = slider.find( 'li' ).length,
		counter = 0;

	$( 'a.more', slider ).on( 'click', function( evt ) {

		evt.preventDefault();

		counter++;

		sliderUl.animate({
			top: -counter * 233 + 'px'
		}, 500);

		if( counter == (reviewsCount / 2) - 1) {
			$(this).hide()
		}

	});
}

$(function() {
	Homepage.promoSlider();
	Homepage.reviews();
})