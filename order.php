<?php
define("PAGEID", "order");
require('include/header.php');
?>

<section class="order-content">

	<article>
		<header>
			<h1>Nezávazná objednávka</h1>
		</header>

		<form action="#" method="post">
		<section class="term">
			<h2>Termín pobytu</h2>
			<p class="date">22. 6. 2013 (sobota) - 29. 6. 2013 (sobota) | 7 nocí</p>
		</section>

		<section class="persons">
			<h2>Informace o cestujících</h2>

				<div class="form-order-box">
					<label for="select-adults">Dospělí</label>
					<select name="select-adults" id="select-adults" class="form-minimalect" data-text-placeholder="Zvolte počet" data-text-empty="Zvolte počet">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="3">4</option>
					</select>
				</div>

				<div class="form-order-box">
					<label for="select-kids">Děti (3 - 12 let)</label>
					<select name="select-kids" id="select-kids" class="form-minimalect" data-text-placeholder="Zvolte počet" data-text-empty="Zvolte počet">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="3">4</option>
					</select>
				</div>

				<div class="form-order-box">
					<label for="select-baby">Batole (0 - 2 roky)</label>
					<select name="select-baby" id="select-baby" class="form-minimalect" data-text-placeholder="Zvolte počet" data-text-empty="Zvolte počet">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="3">4</option>
					</select>
				</div>

				<div class="form-order-box">
					<label for="select-pet">Zvíře (pes, kočka,...)</label>
					<select name="select-pet" id="select-pet" class="form-minimalect" data-text-placeholder="Ne" data-text-empty="Vyberte z možností">
						<option value="no">Ne</option>
						<option value="yes">Ano</option>
					</select>
				</div>
		</section>

		<section class="price">
			<div class="panel-counter">
				<span>doba pobytu a cena</span>
				<div>
					<span class="blue-i"></span>
					<strong>4.615,- Kč</strong>
					<span class="note">za domek na 7 nocí</span>
				</div>
				<ul>
					<li>Uvedené ceny jsou za pronájem domku pro max. 6 osob (včetně dětí).</li>
					<li>Ceny se mohou změnit bez předchozího oznámení s ohledem na kurz.</li>
					<li>Rekreační domky se pronajímají od soboty do soboty, tedy příjezdy jsou v sezóně (Vánoce, Silvestr, leden, únor, červenec, srpen) vždy v sobotu.</li>
					<li>Mimo sezónu se lze dohodnout individuálně</li>
				</ul>
			</div>
		</section>

		<div class="services">
			<div class="options mandatory">
				<h2><span>povinné poplatky</span><em>(placené při příjezdu na recepci)</em></h2>
				<dl>
					<dt>Energie</dt>
					<dd>
						&nbsp;&nbsp;850 Kč / týden (období 1. 5. - 30. 9.)<br>
						1600 Kč / týden (období 1. 10. - 30. 4.)
					</dd>

					<dt>Závěrečný úklid</dt>
					<dd>750 Kč / pobyt</dd>

					<dt>Povlečení (set pro jednu osobu)*</dt>
					<dd>125 Kč / pobyt</dd>

					<dt>Kauce za domek</dt>
					<dd>3.300 Kč / pobyt</dd>

					<dt>Kauce za ovladač pro automatickou závoru</dt>
					<dd>825 Kč / pobyt</dd>

					<dt>Turistická taxa</dt>
					<dd>dle místní vyhlášky, osoba / den, placeno v Kč</dd>
				</dl>
			</div>
			<div class="options optional">
				<h2>volitelné služby</h2>
				<dl>
					<dt><label for="item-order-1" class="chb-label"><input type="checkbox" name="item-order-1" id="item-order-1">Zvíře (pes, kočka,...)</label></dt>
					<dd>375 Kč / týden (max 1 zvíře / dům)</dd>

					<dt><label for="item-order-2" class="chb-label"><input type="checkbox" name="item-order-2" id="item-order-2">Dětská cestovní postylka - nutná rezervace předem</label></dt>
					<dd>325 Kč / pobyt</dd>

					<dt><label for="item-order-3" class="chb-label"><input type="checkbox" name="item-order-3" id="item-order-3">Dětká jídelní židle - nutné rezervace předem</label></dt>
					<dd>325 Kč / pobyt</dd>

					<dt><label for="item-order-4" class="chb-label"><input type="checkbox" name="item-order-4" id="item-order-4">Ručníkový set</label></dt>
					<dd>375 Kč / pobyt</dd>

					<dt><label for="item-order-5" class="chb-label"><input type="checkbox" name="item-order-5" id="item-order-5">Internet Wi-Fi</label></dt>
					<dd>150 Kč / pobyt / 1ks PC</dd>
				</dl>
			</div>
		</div>

		<div class="price">
			<div class="panel-counter">
				<span class="note">cena za domek na 7 nocí včetně volitelných služeb</span>
				<div>
					<span class="blue-i"></span>
					<strong>4.615,- Kč</strong>
				</div>
			</div>
		</div>

		<div class="contact-info">
			<h3>kontakty</h3>

			<label for="item-contact-1">Oslovení Pan / Paní</label>
			<input type="text" name="item-contact-1" id="item-contact-1">
			<label for="item-contact-2">Křestní jméno <sup>*</sup></label>
			<input type="text" name="item-contact-2" id="item-contact-2">
			<label for="item-contact-3">Příjmení <sup>*</sup></label>
			<input type="text" name="item-contact-3" id="item-contact-3">
			<label for="item-contact-4">Ulice <sup>*</sup></label>
			<input type="text" name="item-contact-4" id="item-contact-4">
			<label for="item-contact-5">Upřesnění adresy (nepovinný údaj)</label>
			<input type="text" name="item-contact-5" id="item-contact-5">
			<label for="item-contact-6">PSČ (ve formátu 123 45) <sup>*</sup></label>
			<input type="text" name="item-contact-6" id="item-contact-6">
			<label for="item-contact-7">Město <sup>*</sup></label>
			<input type="text" name="item-contact-7" id="item-contact-7">
			<label for="item-contact-8">Telefon <sup>*</sup></label>
			<input type="text" name="item-contact-8" id="item-contact-8">
			<label for="item-contact-9">E-mail <sup>*</sup></label>
			<input type="text" name="item-contact-9" id="item-contact-9">
			<label for="item-contact-10">Země <sup>*</sup></label>
			<input type="text" name="item-contact-10" id="item-contact-10">

			<label for="item-contact-11" class="chb-label"><input type="checkbox" name="item-contact-11" id="item-contact-11">Ano, mám zájem odebírat Interhome Newsletter a dostávat pravidelně informace o akcích a novinkách. Odhlášení je možné kdykoliv.</label>

			<label for="contact-item-12">Poznámky</label>
			<textarea name="contact-item-12" id="contact-item-12"></textarea>
		</div>

		<a class="button" href="#"><span>Zarezervuj</span></a>

		</form>
	</article>


</section>

<?php
require('include/footer.php');
?>
