# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "images"
javascripts_dir = "scripts"
fonts_dir = "fonts"

environment = :production

#output_style = :expanded
output_style = (environment == :production) ? :compact : :expanded

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false
color_output = false

preferred_syntax = :sass

debug_info = (environment == :production) ? false : true
