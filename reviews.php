<?php
define("PAGEID", "reviews");
require('include/header.php');
?>

<section class="reviews-content">

	<div class="gallery-holder">
		<div class="gallery-placeholder"></div>
		<div class="gallery-thumbs-wrapper">
			<ul class="gallery-thumbs">
				<li data-src="review-1"><span><img src="." data-src="holder.js/250x183/dark" data-rev-id="1" alt=""></span></li>
				<li data-src="review-2"><span><img src="." data-src="holder.js/250x183/industrial" data-rev-id="2" alt=""></span></li>
				<li data-src="review-1"><span><img src="." data-src="holder.js/250x183/social" alt="" data-rev-id="3"></span></li>
				<li data-src="review-2"><span><img src="." data-src="holder.js/250x183/gray" alt=""  data-rev-id="4"></span></li>
			</ul>

			<a href="#" class="prev">&uarr; PREV</a>
			<a href="#" class="next">&darr; NEXT</a>

			<div class="reviews-container">
				<div class="review-item" id="review-1">
					<article>
						<header>
							<h1>Otto Stück</h1>
							<strong>Praha | ČR</strong>
						</header>
						<section class="rating"><em class="stars star-5"></em></section>
						<section class="comment">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam aperiam dolorem maxime non, quibusdam repellat velit veniam! Eius illo illum labore molestiae provident, quaerat quidem quo rem tenetur voluptatum?</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam aperiam dolorem maxime non, quibusdam repellat velit veniam! Eius illo illum labore molestiae provident, quaerat quidem quo rem tenetur voluptatum?</p>
						</section>

						<section class="tips">
							<h2>Tipy na výlet</h2>

							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem culpa ducimus facere id, iste natus omnis pariatur quia sunt. Adipisci alias consectetur hic iusto necessitatibus, temporibus? Odio, praesentium, sequi!</p>
						</section>

					</article>
				</div>

				<div class="review-item" id="review-2">
					<article>
						<header>
							<h1>Alice Kaufmanová</h1>
							<strong>Praha | ČR</strong>
						</header>
						<section class="rating"><em class="stars star-3"></em></section>
						<section class="comment">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam aperiam dolorem maxime non, quibusdam repellat velit veniam! Eius illo illum labore molestiae provident, quaerat quidem quo rem tenetur voluptatum?</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquam aperiam dolorem maxime non, quibusdam repellat velit veniam! Eius illo illum labore molestiae provident, quaerat quidem quo rem tenetur voluptatum?</p>
						</section>

						<section class="tips">
							<h2>Tipy na výlet</h2>

							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem culpa ducimus facere id, iste natus omnis pariatur quia sunt. Adipisci alias consectetur hic iusto necessitatibus, temporibus? Odio, praesentium, sequi!</p>
						</section>

					</article>
				</div>
			</div>

		</div>
	</div>

	<div class="reviews-tabs-wrapper">
		<section class="reviews-tabs tabs">
			<header>
				<h2>Doporučení našich hostů</h2>
				<nav class="tabs-nav">
					<ul>
						<!--<li><a href="#tab-prev"><</a></li>-->
						<li class="active"><a href="#tab-1">1</a></li>
						<li><a href="#tab-2">2</a></li>
						<li><a href="#tab-3">3</a></li>
						<!--<li><a href="#tab-next">></a></li>-->
					</ul>
				</nav>
			</header>
			<div class="tabs-content">
				<div class="tab active" id="tab-1">
					<div class="inner">
						<article class="rating-item">
							<div class="rating-meta">
								<strong>Alice Kaufmanová</strong>
								<span class="city">Praha</span><span class="country">ČR</span>
								<span class="stars-wrapper"><em class="stars star-5"></em></span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
							<img src="images/person-1.jpg" alt="">
						</article>

						<article class="rating-item">
							<div class="rating-meta">
								<strong>Ralf Henning</strong>
								<span class="city">Berlin</span><span class="country">DE</span>
								<span class="stars-wrapper"><em class="stars star-1"></em></span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
							<img src="images/person-2.jpg" alt="">
						</article>

						<article class="rating-item">
							<div class="rating-meta">
								<strong>Alice Kaufmanová</strong>
								<span class="city">Praha</span><span class="country">ČR</span>
								<span class="stars-wrapper"><em class="stars star-5"></em></span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
							<img src="images/person-1.jpg" alt="">
						</article>

						<article class="rating-item">
							<div class="rating-meta">
								<strong>Ralf Henning</strong>
								<span class="city">Berlin</span><span class="country">DE</span>
								<span class="stars-wrapper"><em class="stars star-1"></em></span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
							<img src="images/person-2.jpg" alt="">
						</article>
					</div>
				</div>
				<div class="tab" id="tab-2">
					<div class="inner">
						<article class="rating-item">
							<div class="rating-meta">
								<strong>Alice Kaufmanová 2</strong>
								<span class="city">Praha</span><span class="country">ČR</span>
								<span class="stars-wrapper"><em class="stars star-5"></em></span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
							<img src="images/person-1.jpg" alt="">
						</article>

						<article class="rating-item">
							<div class="rating-meta">
								<strong>Ralf Henning 2</strong>
								<span class="city">Berlin</span><span class="country">DE</span>
								<span class="stars-wrapper"><em class="stars star-1"></em></span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
							<img src="images/person-2.jpg" alt="">
						</article>

						<article class="rating-item">
							<div class="rating-meta">
								<strong>Alice Kaufmanová 2</strong>
								<span class="city">Praha</span><span class="country">ČR</span>
								<span class="stars-wrapper"><em class="stars star-5"></em></span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
							<img src="images/person-1.jpg" alt="">
						</article>

						<article class="rating-item">
							<div class="rating-meta">
								<strong>Ralf Henning 2</strong>
								<span class="city">Berlin</span><span class="country">DE</span>
								<span class="stars-wrapper"><em class="stars star-1"></em></span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
							<img src="images/person-2.jpg" alt="">
						</article>
					</div>
				</div>
				<div class="tab" id="tab-3">
					<div class="inner">
						<article class="rating-item">
							<div class="rating-meta">
								<strong>Alice Kaufmanová 3</strong>
								<span class="city">Praha</span><span class="country">ČR</span>
								<span class="stars-wrapper"><em class="stars star-5"></em></span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
							<img src="images/person-1.jpg" alt="">
						</article>

						<article class="rating-item">
							<div class="rating-meta">
								<strong>Ralf Henning 3</strong>
								<span class="city">Berlin</span><span class="country">DE</span>
								<span class="stars-wrapper"><em class="stars star-1"></em></span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
							<img src="images/person-2.jpg" alt="">
						</article>

						<article class="rating-item">
							<div class="rating-meta">
								<strong>Alice Kaufmanová 3</strong>
								<span class="city">Praha</span><span class="country">ČR</span>
								<span class="stars-wrapper"><em class="stars star-5"></em></span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
							<img src="images/person-1.jpg" alt="">
						</article>

						<article class="rating-item">
							<div class="rating-meta">
								<strong>Ralf Henning 3</strong>
								<span class="city">Berlin</span><span class="country">DE</span>
								<span class="stars-wrapper"><em class="stars star-1"></em></span>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
							<img src="images/person-2.jpg" alt="">
						</article>
					</div>
				</div>
				<!-- <div class="tab" id="tab-4">
					<div class="inner">
						tab 4
					</div>
				</div> -->
			</div>
		</section>
	</div>

	<div class="reviews-slider-wrapper">
		<div class="reviews-slider">
			<div class="inner">
				<article class="slide" id="slide-1">
					<a href="#"><img src="images/review-box-1.png" alt=""></a>
				</article>
				<article class="slide" id="slide-2">
					<a href="#"><img src="images/review-box-2.png" alt=""></a>
				</article>
				<article class="slide" id="slide-3">
					<a href="#"><img src="images/review-box-3.png" alt=""></a>
				</article>
				<article class="slide" id="slide-4">
					<a href="#"><img src="images/review-box-2.png" alt=""></a>
				</article>
				<article class="slide" id="slide-5">
					<a href="#"><img src="images/review-box-1.png" alt=""></a>
				</article>
			</div>
		</div>
	</div>
</section>



<?php
require('include/footer.php');
?>