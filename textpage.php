<?php
define("PAGEID", "textpage");
require('include/header.php');
?>

<section class="textpage-content">

	<section class="boxes no-padding">
		<img src="images/ski.jpg" alt="">

		<div class="title">
			<h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit.<a href="pricelist.php"><span>Zarezervuj si svoje místo</span></a></h1>
		</div>

		<a href="#" class="bubble" style="left: 90px; top: 120px;"><img src="images/textpage-bubble.png" alt=""></a>

	</section>

	<article class="from-editor">
		<section class="textpage-perex">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto assumenda commodi consequatur cumque eius esse est, expedita explicabo inventore ipsa nihil numquam quae quibusdam quis quod repellat sint tenetur vel!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias animi beatae <strong>cumque dolorem et, expedita impedit in maxime molestias natus nihil porro quaerat rem sapiente suscipit</strong>. Consectetur doloribus molestiae praesentium!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque fugit id ipsum praesentium</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus at cumque eum hic id inventore, iste molestiae mollitia neque numquam, praesentium rem, reprehenderit soluta suscipit ut vel veniam voluptate!</p>
		</section>


		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto assumenda commodi consequatur cumque eius esse est, expedita explicabo inventore ipsa nihil numquam quae quibusdam quis quod repellat sint tenetur vel!</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque fugit id ipsum praesentium</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus at cumque eum hic id inventore, iste molestiae mollitia neque numquam, praesentium rem, reprehenderit soluta suscipit ut vel veniam voluptate!</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias animi beatae <strong>cumque dolorem et, expedita impedit in maxime molestias natus nihil porro quaerat rem sapiente suscipit</strong>. Consectetur doloribus molestiae praesentium!</p>

	</article>

</section>



<?php
require('include/footer.php');
?>