<?php
define("PAGEID", "gallery");
require('include/header.php');
?>

<section class="gallery-content">
	<div class="gallery-panel">
		<div class="info">
			<h3>Fotogalerie</h3>
			<div class="select">
				<form action="." method="post">
					<!--
					atributy:
					===
					select:
					name - je jedno co tam bude
					id - nechat, je na to navazany javascript (popr. bude potreba upravit volani JS)
					data-default-value - pokud bude vybrana nejaka defaultni fotogalerie (asi by mela - na kazdy strance ta o cem ta stranka je)
					data-text-placeholder - pokud neni zadna vybrana, tohle je default text, priraveno pro jazykovy verze
					data-text-empty - v selectboxu lze vyhledavat psanim, tohle je hlaska pokud zadna galerie nesouhlasi (podle nazvu)

					option:
					value - identifikator galerie (pouziva se pro identifikaci pripadny defaultne vybrany fotogalerie - viz vyse)
					data-url - URL na kterou se ma presmerovat po vybrani polozky, ted se teda pocita s tim, ze co fotogalerie, tak to samostatna stranka, zejo
					-->
					<select name="x" id="gallery-select" data-default-value="gallery-1" data-text-placeholder="Vyberte fotogalerii" data-text-empty="Nenalezeno">
						<option value="gallery-1" data-url="#">galerie 1</option>
						<option value="gallery-2" data-url="gallery-2.php">galerie 2</option>
					</select>
				</form>
			</div>
		</div>
		<p class="help">klikněte na fotografii pro zvětšení</p>
	</div>

	<div class="gallery-holder">
		<div class="gallery-placeholder"></div>
		<div class="gallery-thumbs-wrapper">
			<ul class="gallery-thumbs">
				<li><span><img src="images/gallery-image-small.jpg" data-target="images/gallery-image-big.jpg" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/industrial" data-target="holder.js/530x395/industrial" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/social" data-target="holder.js/530x395/social" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/gray" data-target="holder.js/530x395/gray" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/#000:#fff" data-target="holder.js/530x395/#000:#fff" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/industrial" data-target="holder.js/530x395/industrial" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/social" data-target="holder.js/530x395/social" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/gray" data-target="holder.js/530x395/gray" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/#000:#fff" data-target="holder.js/530x395/#000:#fff" alt=""></span></li>
			</ul>

			<a href="#" class="prev">&uarr; PREV</a>
			<a href="#" class="next">&darr; NEXT</a>
		</div>
	</div>
</section>

<section class="from-editor">
	<article>
		<section class="from-editor-main">
			<header>
				<h1>Vážení hosté, vítejte na stránkách Villaparku Happy Hill!</h1>
			</header>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, nam blanditiis similique ullam soluta voluptas laudantium ipsam! Minima, officiis, consequuntur aliquam provident explicabo vel optio molestiae quaerat voluptatibus dicta consectetur?</p>

			<h2>Lorem ipsum dolor sit amet</h2>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, nam blanditiis similique ullam soluta voluptas laudantium ipsam! Minima, officiis, consequuntur aliquam provident explicabo vel optio molestiae quaerat voluptatibus dicta consectetur?</p>

			<h3>Lorem ipsum dolor sit amet</h3>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, nam blanditiis similique ullam soluta voluptas laudantium ipsam! Minima, officiis, consequuntur aliquam provident explicabo vel optio molestiae quaerat voluptatibus dicta consectetur?</p>

			<h4>Lorem ipsum dolor sit amet</h4>
			<ul>
				<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
				<li>Cum, nam blanditiis similique ullam soluta voluptas laudantium ipsam! Minima, officiis.</li>
				<li>Consequuntur aliquam provident explicabo vel optio molestiae.</li>
				<li>Quaerat voluptatibus dicta consectetur?.</li>
			</ul>
		</section>
		<aside class="from-editor-column">
			<img src="images/gallery-visual.jpg" alt="šťastná to rodinka...."/>
		</aside>
	</article>
</section>

<?php
	require('include/footer.php');
?>