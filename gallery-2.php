<?php
define("PAGEID", "gallery");
require('include/header.php');
?>

<section class="gallery-content">
	<div class="gallery-panel">
		<div class="info">
			<h3>Fotogalerie</h3>
			<div class="select">
				<form action="." method="post">
					<!--
					atributy:
					===
					select:
					name - je jedno co tam bude
					id - nechat, je na to navazany javascript (popr. bude potreba upravit volani JS)
					data-default-value - pokud bude vybrana nejaka defaultni fotogalerie (asi by mela - na kazdy strance ta o cem ta stranka je)
					data-text-placeholder - pokud neni zadna vybrana, tohle je default text, priraveno pro jazykovy verze
					data-text-empty - v selectboxu lze vyhledavat psanim, tohle je hlaska pokud zadna galerie nesouhlasi (podle nazvu)

					option:
					value - identifikator galerie (pouziva se pro identifikaci pripadny defaultne vybrany fotogalerie - viz vyse)
					data-url - URL na kterou se ma presmerovat po vybrani polozky, ted se teda pocita s tim, ze co fotogalerie, tak to samostatna stranka, zejo
					-->
					<select name="x" id="gallery-select" data-default-value="gallery-2" data-text-placeholder="Vyberte fotogalerii" data-text-empty="Nenalezeno">
						<option value="gallery-1" data-url="gallery.php">galerie 1</option>
						<option value="gallery-2" data-url="#">galerie 2</option>
					</select>
				</form>
			</div>
		</div>
		<p class="help">klikněte na fotografii pro zvětšení</p>
	</div>

	<div class="gallery-holder">
		<div class="gallery-placeholder"></div>
		<div class="gallery-thumbs-wrapper">
			<ul class="gallery-thumbs">
				<li><span><img src="images/gallery-image-small.jpg" data-target="images/gallery-image-big.jpg" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/industrial" data-target="holder.js/530x395/industrial" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/social" data-target="holder.js/530x395/social" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/gray" data-target="holder.js/530x395/gray" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/#000:#fff" data-target="holder.js/530x395/#000:#fff" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/industrial" data-target="holder.js/530x395/industrial" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/social" data-target="holder.js/530x395/social" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/gray" data-target="holder.js/530x395/gray" alt=""></span></li>
				<li><span><img src="." data-src="holder.js/210x155/#000:#fff" data-target="holder.js/530x395/#000:#fff" alt=""></span></li>
			</ul>

			<a href="#" class="prev">&uarr; PREV</a>
			<a href="#" class="next">&darr; NEXT</a>
		</div>
	</div>
</section>

<?php
	require('include/footer.php');
?>