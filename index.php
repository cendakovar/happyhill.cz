<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Happy Hill</title>

	<style>
		body {
			margin: 0;
			padding: 50px;
			font-family: arial, sans-serif;
			color: #666;
		}
		h1 {
			margin-top: 0;
		}
		li {
			line-height: 1.4em
		}
		li.in-progress a {
			text-decoration: line-through;
			color: #cc2222;
		}
		a {
			display: inline-block;
			/*margin-bottom: 5px;*/
			color: #97bee5;
			text-decoration: none;
			-webkit-transition: all .3s;
			-moz-transition: all .3s;
			-ms-transition: all .3s;
			-o-transition: all .3s;
			transition: all .3s;
		}
		a:hover {
			color: #052c55;
			margin-left: 10px
		}

	</style>
</head>
<body>
	<h1>Happy Hill - šablony</h1>

	<ul>
		<li><a href="homepage.php">homepage</a></li>
		<li><a href="gallery.php">galerie s textem</a></li>
		<li><a href="gallery-2.php">galerie bez textu</a></li>
		<li><a href="contacts.php">kontakt</a></li>
		<li><a href="pricelist.php">ceník</a>
			<ul>
				<li><a href="pricelist.php?dateStart">ceník - předvybráno</a></li>
			</ul>
		</li>
		<li><a href="order.php">objednavka</a></li>
		<li><a href="reviews.php">hodnocení</a></li>
		<li><a href="villapark.php">villapark</a>
			<ul>
				<li><a href="villapark-2.php">villapark - obrázek</a></li>
				<li><a href="villapark-3.php">villapark - obrázek bez mezery</a></li>
			</ul>
		</li>
		<li><a href="textpage.php">textová stránka</a></li>
	</ul>

</body>
</html>