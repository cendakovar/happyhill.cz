<?php
define("PAGEID", "pricelist");
require('include/header.php');
?>

<section class="pricelist-content">


	<div class="pricelist-panel">
		<a href="#" data-handler="prev" class="load-prev"><span>předchozí měsíc</span></a>

		<div class="select">
			<form action="." method="post">
				<!--
				atributy:
				===
				select:
				name - je jedno co tam bude
				id - nechat, je na to navazany javascript (popr. bude potreba upravit volani JS)
				data-default-value - pokud bude vybrana nejaka defaultni fotogalerie (asi by mela - na kazdy strance ta o cem ta stranka je)
				data-text-placeholder - pokud neni zadna vybrana, tohle je default text, priraveno pro jazykovy verze
				data-text-empty - v selectboxu lze vyhledavat psanim, tohle je hlaska pokud zadna galerie nesouhlasi (podle nazvu)

				option:
				value - identifikator galerie (pouziva se pro identifikaci pripadny defaultne vybrany fotogalerie - viz vyse)
				data-url - URL na kterou se ma presmerovat po vybrani polozky, ted se teda pocita s tim, ze co fotogalerie, tak to samostatna stranka, zejo
				-->
				<select name="x" class="form-minimalect" id="year-select" data-default-value="<?php echo date('Y') ?>" data-text-placeholder="<?php echo date('Y') ?>" data-text-empty="Datum je mimo rozsah">
					<option value="2013" data-url="2013">2013</option>
					<option value="2014" data-url="2014">2014</option>
					<option value="2015" data-url="2015">2015</option>
				</select>
			</form>
		</div>

		<a href="#" data-handler="next" class="load-next"><span>další měsíc</span></a>
	</div>

	<div class="calendars">
		<script>
			// definice, kdy lze vybrat ktery ty pobytu - libovolny jde vzdy, ostatni dle nize uvedeneho
			// indexy msice jsou cislovany od 1, tedy zari = 9 apod.
			// to je rozdil oproti konfiguraci celeho kalendare, kde je indexace od 0
			window.monthsDefinition = {
				week: [11],
				weekandweekend: [12]
			};

			// pole uz zabranych datumu, ktere nelze znova nakliknout
			// bacha, datum bere mesice jako polozky v poli, tedy indexovany od nuly, cerevenec tak neni 7 ale 6, proto je tam vzdy u mesice snizit index o -1
			// window.disabledDefinition = [
			// 	new Date(2013, 12-1, 13),
			// 	new Date(2013, 12-1, 16),
			// 	new Date(2013, 12-1, 30)
			// ];
			window.disabledDefinition = []

			// pole datumu se specialni akci
			// window.specialsDefinition = [
			// 	new Date(2013, 12-1, 17),
			// 	new Date(2013, 12-1, 18),
			// 	new Date(2013, 12-1, 19)
			// ];
			window.specialsDefinition = [];

		</script>
		<div id="datepicker" data-with-disabled-date="Termín nelze vybrat, protože obsahuje nepovolené datum!" data-unselected-all="Vyberte prosím datum!" data-only-week="V tomto termínu je možno vybrat pouze týdenní pobyt" data-only-weekandweekend="V tomto termínu je možno vybrat pouze víkendový nebo týdenní pobyt"></div>

		<div class="legend">
			<ul>
				<li class="special">speciální akce<span><span>8</span></span></li>
				<li class="free">volný termín<span><span>8</span></span></li>
				<li class="highlight">zvolený termín<span><span>8</span></span></li>
				<li class="disabled">termín není volný<span><span>8</span></span></li>
			</ul>
		</div>

	</div>

	<ul>
		<li>Uvedené ceny jsou za pronájem domku pro max. 6 osob (včetně dětí).</li>
		<li>Ceny se mohou změnit bez předchozího oznámení s ohledem na kurz.</li>
		<li>Rekreační domky se pronajímají od soboty do soboty, tedy příjezdy jsou v sezóně (Vánoce, Silvestr, leden, únor, červenec, srpen) vždy v sobotu.</li>
		<li>Mimo sezónu se lze dohodnout individuálně</li>
	</ul>

	<strong>Příjezd v sobotu: 15:00 - 18:00</strong>
	<strong>Odjezd v sobotu: 7:00 - 10:00</strong>

	<div class="panel-counter">
		<strong class="date">22. 6. 2013 (sobota) - 29. 6. 2013 (sobota) | 7 nocí</strong>
		<span>doba pobytu a cena</span>
		<div>
			<span class="blue-i"></span>
			<strong>4.615,- Kč</strong>
			<a class="button" href="#"><span>Zarezervuj</span></a>
			<span class="note">za domek na 7 nocí</span>
		</div>

		<div>
			<select name="y" class="form-minimalect" id="style-select" data-default-value="" data-text-placeholder="Vyberte interval" data-text-empty="Vyberte interval">
				<option value="week" data-url="#">týden</option>
				<option value="weekend" data-url="#">víkend</option>
				<option value="custom" data-url="#">vlastní</option>
			</select>
		</div>
	</div>

	<div class="services">
		<div class="options mandatory">
			<h2><span>povinné poplatky</span><em>(placené při příjezdu na recepci)</em></h2>
			<dl>
				<dt>Energie</dt>
				<dd>
					&nbsp;&nbsp;850 Kč / týden (období 1. 5. - 30. 9.)<br>
					1600 Kč / týden (období 1. 10. - 30. 4.)
				</dd>

				<dt>Závěrečný úklid</dt>
				<dd>750 Kč / pobyt</dd>

				<dt>Povlečení (set pro jednu osobu)*</dt>
				<dd>125 Kč / pobyt</dd>

				<dt>Kauce za domek</dt>
				<dd>3.300 Kč / pobyt</dd>

				<dt>Kauce za ovladač pro automatickou závoru</dt>
				<dd>825 Kč / pobyt</dd>

				<dt>Turistická taxa</dt>
				<dd>dle místní vyhlášky, osoba / den, placeno v Kč</dd>
			</dl>
		</div>
		<div class="options optional">
			<h2>volitelné služby</h2>
			<dl>
				<dt>Zvíře (pes, kočka,...)</dt>
				<dd>375 Kč / týden (max 1 zvíře / dům)</dd>

				<dt>Dětská cestovní postylka - nutná rezervace předem</dt>
				<dd>325 Kč / pobyt</dd>

				<dt>Dětká jídelní židle - nutné rezervace předem</dt>
				<dd>325 Kč / pobyt</dd>

				<dt>Ručníkový set</dt>
				<dd>375 Kč / pobyt</dd>

				<dt>Internet Wi-Fi</dt>
				<dd>150 Kč / pobyt / 1ks PC</dd>
			</dl>
		</div>
	</div>

</section>

<?php
	require('include/footer.php');
?>