<?php
define("PAGEID", "homepage");
require('include/header.php');
?>

<!-- TEMPLATE START -->
<section class="boxes">
	<section>
		<div class="outer">
			<a href="#">
				<h3 style="color: #ffffff">Tipy na výlet</h3>
				<img src="images/img-trips.jpg" alt="">
			</a>
		</div>
		<div class="outer">
			<a href="contacts.php">
				<h3 style="color: #97bee5">Kde nás najdete</h3>
				<img src="images/img-map.jpg" alt="">
			</a>
		</div>
		<div class="outer video-container" data-videoid="M7lc1UVf-VE">
			<a href="#">
				<h3 style="color: #97bee5">Video tour</h3>
				<img src="images/img-video.jpg" alt="">
				<span></span>
			</a>
			<!--<div id="ytplayer-wrapper"><div id="ytplayer"></div></div>-->
		</div>
		<div class="outer">
			<a href="gallery.php">
				<h3 style="color: #97bee5">Fotogalerie</h3>
				<img src="images/img-photos.jpg" alt="">
			</a>
		</div>
		<div class="outer">
			<a href="#">
				<h3 style="color: #ffffff">Půjčovna lyží</h3>
				<img src="images/img-ski.jpg" alt="">
			</a>
		</div>
		<div class="outer">
			<h3 style="color: #97bee5">Výhody</h3>
			<div class="ico-grid">
				<span class="ico-1" title="WiFi"></span>
				<span class="ico-3" title="Bazén"></span>
				<span class="ico-4" title="Lyžování"></span>
				<span class="ico-2" title="Rodiny s dětmi"></span>
				<span class="ico-6" title="Domácí mazlíci"></span>
				<span class="ico-7" title="Cyklovýlety"></span>
				<span class="ico-5" title="Skibus"></span>
				<span class="ico-9" title="Čerstvý vzdoušek"></span>
				<span class="ico-8" title="Golfík"></span>
			</div>
		</div>
	</section><section>
	<div class="outer">
		<div>
			<h3>Termíny</h3>
			<div class="month">
				<h4>Červen</h4>
				<ul>
					<li><a href="pricelist.php"><span class="date">16. 6. - 13. 6.</span><span class="price">&euro; 295</span></a></li>
					<li><a href="pricelist.php"><span class="date">23. 6. - 30. 6.</span><span class="price">&euro; 295</span></a></li>
				</ul>
			</div>
			<div class="month">
				<h4>Červenec</h4>
				<ul>
					<li><a href="pricelist.php"><span class="date">16. 7. - 13. 7.</span><span class="price">&euro; 295</span></a></li>
					<li><a href="pricelist.php"><span class="date">23. 7. - 30. 7.</span><span class="price">&euro; 295</span></a></li>
					<li><a href="pricelist.php"><span class="date">16. 7. - 13. 7.</span><span class="price">&euro; 295</span></a></li>
					<li><a href="pricelist.php"><span class="date">23. 7. - 30. 7.</span><span class="price">&euro; 295</span></a></li>
				</ul>
			</div>
			<div class="month">
				<h4>Srpen</h4>
				<ul>
					<li><a href="pricelist.php"><span class="date">16. 8. - 13. 8.</span><span class="price">&euro; 295</span></a></li>
				</ul>
			</div>
			<p class="reservation">
				<a href="#"><span>Zarezervuj</span></a>
			</p>
		</div>
	</div><div class="outer">
	<div>
		<h3>Napsali o nás</h3>
		<div class="slider">
			<div class="inner">
				<ul>
					<li>
						<div class="rating-meta">
							<strong>Alice Kaufmanová</strong>
							<span class="city">Praha</span><span class="country">ČR</span>
							<em class="stars star-5"></em>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
						<img src="images/person-1.jpg" alt="">
					</li>
					<li>
						<div class="rating-meta">
							<strong>Ralf Henning</strong>
							<span class="city">Berlin</span><span class="country">DE</span>
							<em class="stars star-1"></em>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis. A, omnis asperiores quisquam ut optio blanditiis.</p>
						<img src="images/person-2.jpg" alt="">
					</li>

					<li>
						<div class="rating-meta">
							<strong>Alice Kaufmanová</strong>
							<span class="city">Praha</span><span class="country">ČR</span>
							<em class="stars star-5"></em>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
						<img src="images/person-1.jpg" alt="">
					</li>
					<li>
						<div class="rating-meta">
							<strong>Ralf Henning</strong>
							<span class="city">Berlin</span><span class="country">DE</span>
							<em class="stars star-1"></em>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
						<img src="images/person-2.jpg" alt="">
					</li>

					<li>
						<div class="rating-meta">
							<strong>Alice Kaufmanová</strong>
							<span class="city">Praha</span><span class="country">ČR</span>
							<em class="stars star-5"></em>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
						<img src="images/person-1.jpg" alt="">
					</li>
					<li>
						<div class="rating-meta">
							<strong>Ralf Henning</strong>
							<span class="city">Berlin</span><span class="country">DE</span>
							<em class="stars star-1"></em>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, omnis asperiores quisquam ut optio blanditiis.</p>
						<img src="images/person-2.jpg" alt="">
					</li>
				</ul>
			</div>
			<a href="#" class="more">více info</a>
		</div>
	</div>
</div>
</section>
</section>
<!-- TEMPLATE END -->


<?php
	require('include/footer.php');
?>